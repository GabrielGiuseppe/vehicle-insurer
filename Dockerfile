FROM adoptopenjdk/openjdk11
ADD target/insurer-0.0.1-SNAPSHOT.jar insurer-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "insurer-0.0.1-SNAPSHOT.jar"]