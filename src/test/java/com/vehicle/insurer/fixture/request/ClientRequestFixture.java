package com.vehicle.insurer.fixture.request;

import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.vehicle.insurer.request.ClientRequest;

import static br.com.six2six.fixturefactory.Fixture.*;

public class ClientRequestFixture implements TemplateLoader {

    public static final String CLIENT_REQUEST = "CLIENT_REQUEST";

    @Override
    public void load() {
        of(ClientRequest.class).addTemplate(CLIENT_REQUEST,
                new Rule(){{
                    add("name", "Gabriel");
                    add("surname", "Giuseppe");
                    add("cpf", 12345678912L);
                    add("city", "Santana de Parnaiba");
                    add("uf", "SP");
                }});
    }
}
