package com.vehicle.insurer.fixture.model;

import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.vehicle.insurer.model.Client;

import static br.com.six2six.fixturefactory.Fixture.of;

public class ClientFixture implements TemplateLoader {

    public static final String CLIENT = "CLIENT";
    @Override
    public void load() {
        of(Client.class).addTemplate(CLIENT,
                new Rule(){{
                    add("uuid", "24c6bb0d-61ed-4093-bb17-ad57d396012f");
                    add("name", "Gabriel Giuseppe");
                    add("cpf", 32145678987L);
                    add("city", "Santana de Parnaiba");
                    add("uf", "SP");
                }});
    }
}
