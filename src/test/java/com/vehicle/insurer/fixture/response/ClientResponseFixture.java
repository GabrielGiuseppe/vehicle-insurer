package com.vehicle.insurer.fixture.response;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.vehicle.insurer.response.ClientResponse;

public class ClientResponseFixture implements TemplateLoader {

    public static final String CLIENT_RESPONSE = "CLIENT_RESPONSE";
    @Override
    public void load() {
        Fixture.of(ClientResponse.class).addTemplate(CLIENT_RESPONSE,
                new Rule(){{
                    add("uuid", "24c6bb0d-61ed-4093-bb17-ad57d396012f");
                    add("name", "Gabriel Giuseppe");
                    add("cpf", 32165498712L);
                    add("city", "Santana de Parnaiba");
                    add("uf", "SP");
                }});
    }
}
