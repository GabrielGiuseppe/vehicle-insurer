package com.vehicle.insurer;

import org.junit.Before;
import org.junit.BeforeClass;

import static br.com.six2six.fixturefactory.loader.FixtureFactoryLoader.loadTemplates;
import static org.mockito.MockitoAnnotations.initMocks;

public class BaseTest {

    @BeforeClass
    public static void setUp() {
        loadTemplates("com.vehicle.insurer.fixture");
    }

    @Before
    public void before() {
        initMocks(this);
    }
}
