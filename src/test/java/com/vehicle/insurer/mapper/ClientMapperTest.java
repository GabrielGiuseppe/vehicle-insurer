package com.vehicle.insurer.mapper;

import br.com.six2six.fixturefactory.Fixture;
import com.vehicle.insurer.BaseTest;
import com.vehicle.insurer.fixture.model.ClientFixture;
import com.vehicle.insurer.fixture.request.ClientRequestFixture;
import com.vehicle.insurer.model.Client;
import com.vehicle.insurer.request.ClientRequest;
import com.vehicle.insurer.response.ClientResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import static br.com.six2six.fixturefactory.Fixture.*;
import static org.junit.Assert.*;


public class ClientMapperTest extends BaseTest {


    @InjectMocks
    private ClientMapper mapper;

    @Before
    public void init(){
        this.mapper = new ClientMapper();
    }

    @Test
    public void mustConvertModelInToResponse() throws Exception{
        Client model = from(Client.class).gimme(ClientFixture.CLIENT);
        ClientResponse response = mapper.response(model);

        assertEquals(response.uuid, model.uuid);
        assertEquals(response.name, model.name);
        assertEquals(response.cpf, model.cpf);
        assertEquals(response.city, model.city);
        assertEquals(response.uf, model.uf);
    }

    @Test
    public void mustConvertClientRequestInToModel()throws Exception{
        ClientRequest request = from(ClientRequest.class).gimme(ClientRequestFixture.CLIENT_REQUEST);
        Client model = mapper.create(request);

        assertEquals(model.name, request.name + " " + request.surname);
        assertEquals(model.cpf, request.cpf);
        assertEquals(model.city, request.city);
        assertEquals(model.uf, request.uf);
    }

}