package com.vehicle.insurer.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan("com.vehicle.insurer")
@EnableJpaRepositories("com.vehicle.insurer")
@EnableAsync
@EnableTransactionManagement
public class InsurerConfiguration {
}
