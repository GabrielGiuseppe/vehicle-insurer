package com.vehicle.insurer.model;

import com.vehicle.insurer.model.common.BaseModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name= "policy")
public class Policy extends BaseModel<Policy> {

    private static final Long NUMBER_CAP = 1000000000L;
    private static final Long NUMBER_START = 0L;

    private static final long serialVersionUID = 6053188997134608253L;

    @Column(nullable = false)
    public String uuid;

    @Column(nullable = false)
    public Long policyNumber;

    @Column(nullable = false)
    public LocalDateTime validityStart;

    @Column(nullable = false)
    public LocalDateTime validityEnd;

    @Column(nullable = false)
    public String vehiclePlate;

    @Column(nullable = false)
    public Long value;


    @PrePersist
    public void initializeUUID() {
        if (Strings.isBlank(uuid)) {
            uuid = "PLC-" + UUID.randomUUID().toString();
        }
        if (Objects.isNull(policyNumber)) {
            Long value = new Random().nextLong();
            if(value<0L){value *= -1L;}
            policyNumber = value;
        }
    }
}