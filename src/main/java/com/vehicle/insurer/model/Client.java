package com.vehicle.insurer.model;

import com.vehicle.insurer.model.common.BaseModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name= "client")
public class Client extends BaseModel<Client>{

    private static final long serialVersionUID = 760883475759712295L;

    @Column(nullable = false)
    public String uuid;

    @Column(nullable = false)
    public String name;

    @Column(nullable = false)
    public Long cpf;

    @Column(nullable = false)
    public String city;

    @Column(nullable = false)
    public String uf;


    @PrePersist
    public void initializeUUID() {
        if (Strings.isBlank(uuid)) {
            uuid = "CLI-" + UUID.randomUUID().toString();
        }
    }
}
