package com.vehicle.insurer.model.dto;

import com.vehicle.insurer.model.Policy;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PolicyFilteredDTO {

    public Boolean isOnDue;

    public Long daysToDueDate;

    public Long daysPastDueDate;

    public Policy model;

    public PolicyFilteredDTO(Policy model){
        this.model = model;
    }

}
