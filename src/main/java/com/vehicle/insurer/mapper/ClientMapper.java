package com.vehicle.insurer.mapper;

import com.vehicle.insurer.model.Client;
import com.vehicle.insurer.request.ClientRequest;
import com.vehicle.insurer.response.ClientResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientMapper {

    public ClientResponse response(Client model){
        //initiating response
        ClientResponse response = new ClientResponse();
        //converting model into the response
        response.uuid = model.uuid;
        response.name = model.name;
        response.cpf = model.cpf;
        response.city = model.city;
        response.uf = model.uf;
        //returning response
        return response;
    }

    public List<ClientResponse> response(List<Client> model){
        //converting all clients in a list to response
        List<ClientResponse> response = model.stream().map(m -> this.response(m)).collect(Collectors.toList());
        //returning all responses in a list
        return response;
    }

    public Client create(ClientRequest request){
        //initiating a new model
        Client model = new Client();
        //converting request into the model
        model.name = request.name + " " + request.surname;
        model.cpf = request.cpf;
        model.city = request.city;
        model.uf = request.uf;
        //returning the model
        return model;
    }

    public Client update(ClientRequest request, Client model){
        //converting a update request into a existing model
        model.name = request.name + " " + request.surname;
        model.cpf = request.cpf;
        model.city = request.city;
        model.uf = request.uf;
        //returning the updated model;
        return model;
    }

}
