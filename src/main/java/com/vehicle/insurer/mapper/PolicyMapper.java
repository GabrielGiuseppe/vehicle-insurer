package com.vehicle.insurer.mapper;

import com.vehicle.insurer.model.Policy;
import com.vehicle.insurer.model.PolicyFilteredResponse;
import com.vehicle.insurer.model.dto.PolicyFilteredDTO;
import com.vehicle.insurer.request.PolicyRequest;
import com.vehicle.insurer.response.PolicyResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.vehicle.insurer.util.Dates.dateToLocalDateTime;
import static com.vehicle.insurer.util.Dates.localDateTimetoDate;

@Service
public class PolicyMapper {

    public PolicyResponse response(Policy model){
        //initiating response
        PolicyResponse response = new PolicyResponse();
        //converting model into the response
        response.policyNumber = model.getPolicyNumber();
        response.validityStart = localDateTimetoDate(model.getValidityStart());
        response.validityEnd = localDateTimetoDate(model.validityEnd);
        response.vehiclePlate = model.vehiclePlate;
        response.value = model.value;
        //returning response
        return response;
    }

    public List<PolicyResponse> response(List<Policy> model){
        //converting all Policies in a list to response
        List<PolicyResponse> response = model.stream().map(m -> this.response(m)).collect(Collectors.toList());
        //returning all responses in a list
        return response;
    }

    public PolicyFilteredResponse filtredResponse(PolicyFilteredDTO dto){
        //initiating a new filtered response
        PolicyFilteredResponse response = new PolicyFilteredResponse();
        response.isOnDue = dto.isOnDue;
        response.daysToDueDate = dto.daysToDueDate;
        response.daysPastDueDate = dto.daysPastDueDate;
        response.vehiclePlate = dto.model.vehiclePlate;
        response.policyValue = dto.model.value;
        //returning response
        return response;
    }

    public Policy create(PolicyRequest request){
        //initiating a new model
        Policy model = new Policy();
        //converting request into the model
        model.validityStart = dateToLocalDateTime(request.validityStart);
        model.validityEnd = dateToLocalDateTime(request.validityEnd);
        model.vehiclePlate = request.vehiclePlate;
        model.value = request.value;
        //returning the model
        return model;
    }

    public Policy update(PolicyRequest request, Policy model){
        //converting a update request into a existing model
        model.validityStart = dateToLocalDateTime(request.validityStart);
        model.validityEnd = dateToLocalDateTime(request.validityEnd);
        model.vehiclePlate = request.vehiclePlate;
        model.value = request.value;
        //returning the updated model;
        return model;
    }
}
