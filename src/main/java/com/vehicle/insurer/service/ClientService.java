package com.vehicle.insurer.service;

import com.vehicle.insurer.request.ClientRequest;
import com.vehicle.insurer.response.ClientResponse;
import com.vehicle.insurer.response.common.BaseReturn;

import java.util.List;

public interface ClientService {

    BaseReturn<List<ClientResponse>> list();

    BaseReturn<ClientResponse> create(ClientRequest request);

    BaseReturn<ClientResponse> update(String uuid, ClientRequest request);
}
