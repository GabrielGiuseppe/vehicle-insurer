package com.vehicle.insurer.service;

public interface AuthenticationService {

    public void basicAuth(String authorization);
}
