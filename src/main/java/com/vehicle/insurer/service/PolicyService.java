package com.vehicle.insurer.service;

import com.vehicle.insurer.model.PolicyFilteredResponse;
import com.vehicle.insurer.request.PolicyRequest;
import com.vehicle.insurer.response.PolicyResponse;
import com.vehicle.insurer.response.common.BaseReturn;

import java.util.List;

public interface PolicyService {

    BaseReturn<List<PolicyResponse>> list();

    BaseReturn<PolicyResponse> create(PolicyRequest request);

    BaseReturn<PolicyResponse> update(Long policyNumber, PolicyRequest request);

    BaseReturn<PolicyFilteredResponse> findByPolicyNumber(Long policyNumber);
}
