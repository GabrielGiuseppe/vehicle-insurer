package com.vehicle.insurer.service.impl;

import com.vehicle.insurer.exception.BaseException;
import com.vehicle.insurer.mapper.ClientMapper;
import com.vehicle.insurer.model.Client;
import com.vehicle.insurer.repository.ClientRepository;
import com.vehicle.insurer.request.ClientRequest;
import com.vehicle.insurer.response.ClientResponse;
import com.vehicle.insurer.response.common.BaseReturn;
import com.vehicle.insurer.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepository repository;
    @Autowired
    private ClientMapper mapper;

    @Override
    public BaseReturn<List<ClientResponse>> list() {
        //finding all clients
        List<Client> model = repository.findAll();
        //populating client response list
        List<ClientResponse> data = mapper.response(model);
        //creating the base return
        BaseReturn<List<ClientResponse>> response = new BaseReturn<List<ClientResponse>>(data);
        //returning response
        return response;
    }

    @Override
    public BaseReturn<ClientResponse> create(ClientRequest request) {
        //verifying the existence of this cpf in the DB
        Boolean exists = repository.existsByCpf(request.cpf);
        //validating existence and trhowing a exception
        if(exists){
            throw new BaseException(BAD_REQUEST, "This document is already registered");
        }
        //populating the model object
        Client model = mapper.create(request);
        //saving the model in the DB
        model = repository.save(model);
        //populating the response
        ClientResponse data = mapper.response(model);
        //creating the base response
        BaseReturn<ClientResponse> response = new BaseReturn<>(data);
        //returning response
        return response;
    }

    @Override
    public BaseReturn<ClientResponse> update(String uuid, ClientRequest request) {
        //finding client by uuid
        Client model = repository.findByUuid(uuid);
        //validating client existence
        if(Objects.isNull(model)){
            throw new BaseException(NOT_FOUND, "Client not found");
        }
        //validating Client data integrity
        if (model.cpf != request.cpf) {
            Boolean exists = repository.existsByCpf(request.cpf);
            if (!exists) {
                throw new BaseException(BAD_REQUEST, "You cannot update a client CPF");
            }
        }
        //updating the existing model
        model = mapper.update(request, model);
        //saving the updated model
        model = repository.save(model);
        //populating client response
        ClientResponse data = mapper.response(model);
        //creating base response
        BaseReturn<ClientResponse> response = new BaseReturn<ClientResponse>(data);
        //returning response
        return response;
    }
}
