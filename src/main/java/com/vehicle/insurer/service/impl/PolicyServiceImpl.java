package com.vehicle.insurer.service.impl;

import com.vehicle.insurer.exception.BaseException;
import com.vehicle.insurer.mapper.PolicyMapper;
import com.vehicle.insurer.model.Policy;
import com.vehicle.insurer.model.PolicyFilteredResponse;
import com.vehicle.insurer.model.dto.PolicyFilteredDTO;
import com.vehicle.insurer.repository.PolicyRepository;
import com.vehicle.insurer.request.PolicyRequest;
import com.vehicle.insurer.response.PolicyResponse;
import com.vehicle.insurer.response.common.BaseReturn;
import com.vehicle.insurer.service.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

import static com.vehicle.insurer.util.Dates.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class PolicyServiceImpl implements PolicyService {

    @Autowired
    private PolicyRepository repository;

    @Autowired
    private PolicyMapper mapper;

    @Override
    public BaseReturn<List<PolicyResponse>> list() {
        //finding all Policys
        List<Policy> model = repository.findAll();
        //populating Policy response list
        List<PolicyResponse> data = mapper.response(model);
        //creating the base return
        BaseReturn<List<PolicyResponse>> response = new BaseReturn<List<PolicyResponse>>(data);
        //returning response
        return response;
    }

    @Override
    public BaseReturn<PolicyResponse> create(PolicyRequest request) {

        //populating the model object
        Policy model = mapper.create(request);
        //saving the model in the DB
        model = repository.save(model);
        //populating the response
        PolicyResponse data = mapper.response(model);
        //creating the base response
        BaseReturn<PolicyResponse> response = new BaseReturn<>(data);
        //returning response
        return response;
    }

    @Override
    public BaseReturn<PolicyResponse> update(Long policyNumber, PolicyRequest request) {
        //finding Policy by uuid
        Policy model = repository.findByPolicyNumber(policyNumber);
        //validating Policy existence
        if(Objects.isNull(model)){
            throw new BaseException(NOT_FOUND, "Policy not found");
        }
        //updating the existing model
        model = mapper.update(request, model);
        //saving the updated model
        model = repository.save(model);
        //populating Policy response
        PolicyResponse data = mapper.response(model);
        //creating base response
        BaseReturn<PolicyResponse> response = new BaseReturn<PolicyResponse>(data);
        //returning response
        return response;
    }

    @Override
    public BaseReturn findByPolicyNumber(Long policyNumber) {
        //finding policy by policy number
        Policy model = repository.findByPolicyNumber(policyNumber);
        //validating Policy existence
        if(Objects.isNull(model)){
            throw new BaseException(NOT_FOUND, "Policy not found");
        }
        //creating a helper DTO
        PolicyFilteredDTO dto = new PolicyFilteredDTO(model);
        //validating if is up to date
        Boolean isOnDue = isOnDue(model, LocalDateTime.now());
        //inserting boolean in the dto
        dto.isOnDue = isOnDue;
        //calculating due dates values
        if(isOnDue){
            dto.daysToDueDate = calculateDaysToDueDate(model, LocalDateTime.now());
            dto.daysPastDueDate = 0L;
        }else{
            dto.daysPastDueDate = calculatedDaysPastDueDat(model, LocalDateTime.now());
            dto.daysToDueDate = 0L;
        }
        //creating police filtered response
        PolicyFilteredResponse data = mapper.filtredResponse(dto);
        //creating base response
        BaseReturn<PolicyFilteredResponse> response = new BaseReturn<>(data);
        //returning response
        return response;
    }

    public Boolean isOnDue(Policy model, LocalDateTime presentTime){
        if(presentTime.isBefore(model.validityEnd)){
            return true;
        }
        return false;
    }

    private Long calculateDaysToDueDate(Policy model, LocalDateTime presentTime) {
        return ChronoUnit.DAYS.between(presentTime, model.validityEnd);
    }

    private Long calculatedDaysPastDueDat(Policy model, LocalDateTime presentTime) {
        return ChronoUnit.DAYS.between(model.validityEnd, presentTime);
    }
}
