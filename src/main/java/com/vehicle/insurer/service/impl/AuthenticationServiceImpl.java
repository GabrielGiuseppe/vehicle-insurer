package com.vehicle.insurer.service.impl;

import com.vehicle.insurer.exception.BaseException;
import com.vehicle.insurer.service.AuthenticationService;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    public static final String AUTHORIZATION_TOKEN = "fc883d24-40d6-45fb-bae0-660924033da2";


    @Override
    public void basicAuth(String authorization) {
        if (isNull(authorization) || !authorization.equals(AUTHORIZATION_TOKEN)) {
            throw new BaseException(UNAUTHORIZED, "Invalid Authorization token");
        }
    }
}
