package com.vehicle.insurer.repository;

import com.vehicle.insurer.model.Client;
import com.vehicle.insurer.model.Policy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolicyRepository extends CrudRepository<Policy, Long> {

    @Override
    List<Policy> findAll();

    Policy findByUuid(String uuid);

    Policy findByPolicyNumber(Long policyNumber);
}
