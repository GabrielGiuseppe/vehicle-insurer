package com.vehicle.insurer.repository;

import com.vehicle.insurer.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    @Override
    List<Client> findAll();

    Client findByUuid(String uuid);

    Boolean existsByCpf(Long cpf);
}
