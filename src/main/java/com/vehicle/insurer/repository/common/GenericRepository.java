package com.vehicle.insurer.repository.common;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface GenericRepository <T> extends CrudRepository<T, Long> {

    @Override
    List<T> findAll();
}
