package com.vehicle.insurer.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PolicyFilteredResponse {

    public Boolean isOnDue;

    public Long daysToDueDate;

    public Long daysPastDueDate;

    public String vehiclePlate;

    public Long policyValue;

}
