package com.vehicle.insurer.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
public class PolicyResponse {

    public Long policyNumber;

    public String validityStart;

    public String validityEnd;

    public String vehiclePlate;

    public Long value;
}
