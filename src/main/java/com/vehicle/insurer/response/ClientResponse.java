package com.vehicle.insurer.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientResponse {

    public String uuid;

    public String name;

    public Long cpf;

    public String city;

    public String uf;

}
