package com.vehicle.insurer.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.vehicle.insurer.controller.comon.BaseController;
import com.vehicle.insurer.model.PolicyFilteredResponse;
import com.vehicle.insurer.request.PolicyRequest;
import com.vehicle.insurer.response.ClientResponse;
import com.vehicle.insurer.response.PolicyResponse;
import com.vehicle.insurer.response.common.BaseReturn;
import com.vehicle.insurer.service.AuthenticationService;
import com.vehicle.insurer.service.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/policy")
public class PolicyController extends BaseController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private PolicyService policyService;

    @Autowired
    private PolicyService service;

    @GetMapping(produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<List<PolicyResponse>> list(@Valid @RequestHeader String authorization){
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //listing all policies
        BaseReturn<List<PolicyResponse>> response = service.list();
        return response;

    }

    @GetMapping(path = "/{policyNumber}", produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<PolicyFilteredResponse> getByPolicynumber(@Valid @RequestHeader String authorization,
                                                                            @PathVariable Long policyNumber){
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //finding policy by policy number
        BaseReturn<PolicyFilteredResponse> response = service.findByPolicyNumber(policyNumber);
        //returning reponse
        return response;
    }

    @PostMapping(produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
        public BaseReturn<PolicyResponse> create(@Valid @RequestHeader String authorization,
                @Valid @RequestBody PolicyRequest request){
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //creating policy
        BaseReturn<PolicyResponse> response = service.create(request);
        //returning created policy
        return response;

    }

    @PutMapping(path = "/{policyNumber}", produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<PolicyResponse> update(@Valid @RequestHeader String authorization,
                                             @RequestBody @Validated PolicyRequest request,
                                             @PathVariable(name = "policyNumber") Long policyNumber) {
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //updating selected policy
        BaseReturn<PolicyResponse> response = service.update(policyNumber, request);
        //returning updated policy
        return response;
    }
}
