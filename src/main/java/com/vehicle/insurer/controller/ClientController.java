package com.vehicle.insurer.controller;

import com.vehicle.insurer.controller.comon.BaseController;
import com.vehicle.insurer.request.ClientRequest;
import com.vehicle.insurer.response.ClientResponse;
import com.vehicle.insurer.response.common.BaseReturn;
import com.vehicle.insurer.service.AuthenticationService;
import com.vehicle.insurer.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController extends BaseController {

    @Autowired
    private ClientService service;
    @Autowired
    AuthenticationService authenticationService;

    @GetMapping(produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<List<ClientResponse>> list(@Valid @RequestHeader String authorization){
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //listing all clients
        BaseReturn<List<ClientResponse>> response = service.list();
        return response;

    }


    @PostMapping(produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<ClientResponse> create(@Valid @RequestHeader String authorization,
                                             @Valid @RequestBody ClientRequest request){
        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //creating client
        BaseReturn<ClientResponse> response = service.create(request);
        //returning created client
        return response;

    }
    @PutMapping(path = "/{uuid}", produces = PRODUCES_JSON, consumes = CONSUMES_JSON)
    public BaseReturn<ClientResponse> update(@Valid @RequestHeader String authorization,
                                             @Valid @RequestBody ClientRequest request,
                                             @PathVariable(name = "uuid") String uuid){

        //Verify authorization token
        authenticationService.basicAuth(authorization);
        //updating selected client
        BaseReturn<ClientResponse> response = service.update(uuid, request);
        //returning updated client
        return response;

    }
}
