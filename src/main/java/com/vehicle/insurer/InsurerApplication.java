package com.vehicle.insurer;

import com.vehicle.insurer.util.TimeZones;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
public class InsurerApplication extends SpringBootServletInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(InsurerApplication.class);

	@PostConstruct
	void started() {
		TimeZones.setUTC();
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(InsurerApplication.class, args);
		String profile = Arrays.toString(ctx.getEnvironment().getActiveProfiles());
		LOGGER.info("Using profile {}", profile);
	}

}
