CREATE TABLE `client` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                          `uuid` varchar(100) NOT NULL,
                          `name` varchar(100) NOT NULL,
                          `cpf` bigint NOT NULL,
                          `city` varchar(100) NOT NULL,
                          `UF` varchar(2) NOT NULL,
                          `created_at` datetime NOT NULL,
                          `updated_at` datetime NOT NULL,
                          `version` int(11) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `policy` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                          `uuid` varchar(100) NOT NULL,
                          `policy_number` bigint NOT NULL,
                          `validity_start` datetime NOT NULL,
                          `validity_end` datetime NOT NULL,
                          `vehicle_plate` varchar(7) NOT NULL,
                          `value` integer(7) NOT NULL,
                          `created_at` datetime NOT NULL,
                          `updated_at` datetime NOT NULL,
                          `version` int(11) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;